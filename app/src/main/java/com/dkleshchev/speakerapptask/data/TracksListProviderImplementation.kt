package com.dkleshchev.speakerapptask.data

import android.os.Environment
import com.dkleshchev.speakerapptask.domain.gateways.TracksListProvider
import com.dkleshchev.speakerapptask.presentation.di.qualifier.NewTrackAddedEvents
import com.dkleshchev.speakerapptask.presentation.main.model.TrackItem
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.io.File
import javax.inject.Inject

class TracksListProviderImplementation @Inject constructor(@NewTrackAddedEvents private val newFileEventsListener: PublishSubject<Any>) :
        TracksListProvider {

    init {
        newFileEventsListener.subscribe { refreshList() }
    }

    private val itemsSubject = BehaviorSubject.create<List<TrackItem>>()

    override fun getTracks(): Observable<List<TrackItem>> {
        refreshList()
        return itemsSubject
    }

    private fun refreshList() {
        tracksDir.listFiles()
                .map { TrackItem(it.name, it.absolutePath, false, 0, 0) }
                .let { itemsSubject.onNext(it) }
    }

    private val tracksDir = Environment.getExternalStorageDirectory().path
            .let { File(it, FileSystemConstants.audioRecorderFolder) }
            .apply {
                if (!exists()) {
                    mkdirs()
                }
            }

}
