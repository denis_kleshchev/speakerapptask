package com.dkleshchev.speakerapptask.data

import android.media.MediaRecorder
import com.dkleshchev.speakerapptask.domain.gateways.AudioRecorder
import javax.inject.Inject

class SimpleAudioRecorderImplementation @Inject constructor() : AudioRecorder {

    private var recorder: MediaRecorder? = null

    override fun startRecord(filename: String) {
        recorder = MediaRecorder().apply {
            setAudioChannels(1)
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setOutputFile(filename)
            setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
            setAudioSamplingRate(44100)
            setAudioEncodingBitRate(96000)
            prepare()
            start()
        }
    }

    override fun stopRecord() {
        recorder?.stop()
    }

}
