package com.dkleshchev.speakerapptask.data

import android.os.Environment
import com.dkleshchev.speakerapptask.domain.gateways.FilenameProvider
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class FilenameProviderImplementation @Inject constructor() : FilenameProvider {

    companion object {

        private const val audioRecorderTempFile = "audiorecordtest.mp4"
        private const val filePathSeparator = "/"
        private const val wavExtension = ".mp4"
        private const val dateFormat = "dd.MMMM HH:mm"

    }

    private val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())

    override fun getTempFileName(): String {
        val filepath = Environment.getExternalStorageDirectory().path
        val file = File(filepath, FileSystemConstants.audioRecorderFolder)

        if (!file.exists()) {
            file.mkdirs()
        }

        val tempFile = File(filepath, audioRecorderTempFile)

        if (tempFile.exists())
            tempFile.delete()

        return file.absolutePath + filePathSeparator + audioRecorderTempFile
    }

    override fun getFileName(): String {
        val file = Environment.getExternalStorageDirectory().path
                .let { File(it, FileSystemConstants.audioRecorderFolder) }

        if (!file.exists()) {
            file.mkdirs()
        }

        return file.absolutePath + filePathSeparator + formatter.format(Date()) + wavExtension
    }

}
