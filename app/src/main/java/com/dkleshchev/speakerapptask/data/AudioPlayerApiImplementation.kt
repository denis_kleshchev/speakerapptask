package com.dkleshchev.speakerapptask.data

import android.media.AudioAttributes
import android.media.MediaPlayer
import com.dkleshchev.speakerapptask.domain.gateways.AudioPlayerApi
import com.dkleshchev.speakerapptask.logInfo
import com.dkleshchev.speakerapptask.presentation.di.qualifier.CurrentProgressEvents
import com.dkleshchev.speakerapptask.presentation.di.qualifier.StopPlaybackEvents
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.io.FileInputStream
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

class AudioPlayerApiImplementation @Inject constructor(@CurrentProgressEvents private val progressListener: BehaviorSubject<Pair<Long, Int>>,
                                                       @StopPlaybackEvents private val stopEvents: PublishSubject<Any>) :
        AudioPlayerApi {

    init {
        stopEvents.subscribe {
            releasePlayer()
            releaseProgress()
        }
    }

    private val player = MediaPlayer().apply {
        setAudioAttributes(AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_MUSIC).build())
    }

    private val playCompleted = AtomicBoolean(false)

    private var dataSource: String = ""
        set(value) {
            logInfo("start playing, the same file: ${field == value}, is player completed: ${playCompleted.get()}")
            if (field != value || playCompleted.get()) {
                field = value
                setNewSourceAndPlay()
                releaseProgress()
                initializeProgress()
            } else {
                logInfo("is playing: ${player.isPlaying}")
                if (player.isPlaying) {
                    player.pause()
                } else {
                    startPlaying()
                }
            }
        }

    private val timer = Observable.interval(50, TimeUnit.MILLISECONDS)

    private var progressSubscription: Disposable? = null

    override fun playOrPause(file: String) {
        dataSource = file
    }

    override fun seek(progress: Int) {
        player.seekTo(progress)
    }

    private fun setNewSourceAndPlay() {
        player.apply {
            FileInputStream(dataSource).use {
                setDataSource(it.fd)
                setOnCompletionListener { releasePlayer() }
                prepare()
                startPlaying()
            }
        }
    }

    private fun initializeProgress() {
        progressSubscription = timer
                .subscribe {
                    progressListener.onNext(Pair(player.currentPosition.toLong(), player.duration))
                }
    }

    private fun startPlaying() {
        playCompleted.set(false)
        player.start()
    }

    private fun releaseProgress() {
        progressSubscription?.let {
            if (it.isDisposed.not()) {
                it.dispose()
            }
        }
    }

    private fun releasePlayer() {
        playCompleted.set(true)
        player.apply {
            releaseProgress()
            reset()
        }
    }

}
