package com.dkleshchev.speakerapptask.data

import com.dkleshchev.speakerapptask.domain.gateways.FileHelper
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import javax.inject.Inject

class FileHelperImplementation @Inject constructor() :
        FileHelper {

    override fun copyFile(input: String, output: String) {
        val data = ByteArray(2048)

        FileInputStream(input).use { inputStream ->
            FileOutputStream(output).use { outputStream ->
                while (inputStream.read(data) != -1) {
                    outputStream.write(data)
                }
            }
        }
    }

    override fun deleteFile(filename: String) {
        File(filename).apply {
            if (exists()) {
                delete()
            }
        }
    }

}
