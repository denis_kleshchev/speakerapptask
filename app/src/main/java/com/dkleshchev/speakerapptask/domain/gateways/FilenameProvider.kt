package com.dkleshchev.speakerapptask.domain.gateways

interface FilenameProvider {

    fun getTempFileName(): String

    fun getFileName(): String

}
