package com.dkleshchev.speakerapptask.domain.gateways

interface AudioPlayerApi {

    fun playOrPause(file: String)

    fun seek(progress: Int)

}
