package com.dkleshchev.speakerapptask.domain.boundaries

import com.dkleshchev.speakerapptask.presentation.main.model.ButtonEvent
import com.dkleshchev.speakerapptask.presentation.main.MainViewState
import io.reactivex.Observable

interface AudioRecorderApi {

    fun processButtonEvent(): (ButtonEvent) -> Observable<MainViewState>

}