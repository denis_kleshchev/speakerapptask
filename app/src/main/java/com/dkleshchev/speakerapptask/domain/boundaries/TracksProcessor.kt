package com.dkleshchev.speakerapptask.domain.boundaries

import com.dkleshchev.speakerapptask.presentation.main.model.TrackData
import com.dkleshchev.speakerapptask.presentation.main.tracks.TracksViewState
import io.reactivex.Observable

interface TracksProcessor {

    fun processEvents() : (TrackData) -> Observable<TracksViewState>

}

/*
*

    private val storagePermissionsRequest by lazy {
        TedRx2Permission.with(context)
                .setRationaleTitle(R.string.rationale_title)
                .setRationaleMessage(R.string.rationale_storage_text)
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .request()
    }
*
* */