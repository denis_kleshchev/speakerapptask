package com.dkleshchev.speakerapptask.domain.gateways

import com.dkleshchev.speakerapptask.presentation.main.model.TrackItem
import io.reactivex.Observable

interface TracksListProvider {

    fun getTracks() : Observable<List<TrackItem>>

}
