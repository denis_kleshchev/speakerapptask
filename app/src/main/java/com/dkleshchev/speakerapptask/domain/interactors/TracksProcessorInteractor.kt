package com.dkleshchev.speakerapptask.domain.interactors

import android.Manifest
import android.content.Context
import com.dkleshchev.speakerapptask.R
import com.dkleshchev.speakerapptask.domain.boundaries.TracksProcessor
import com.dkleshchev.speakerapptask.domain.gateways.AudioPlayerApi
import com.dkleshchev.speakerapptask.domain.gateways.TracksListProvider
import com.dkleshchev.speakerapptask.presentation.di.qualifier.CurrentProgressEvents
import com.dkleshchev.speakerapptask.presentation.main.model.TrackData
import com.dkleshchev.speakerapptask.presentation.main.tracks.TracksViewState
import com.tedpark.tedpermission.rx2.TedRx2Permission
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class TracksProcessorInteractor @Inject constructor(private val context: Context,
                                                    private val filesProvider: TracksListProvider,
                                                    private val player: AudioPlayerApi,
                                                    @CurrentProgressEvents private val progressListener: BehaviorSubject<Pair<Long, Int>>) :
        TracksProcessor {

    override fun processEvents(): (TrackData) -> Observable<TracksViewState> {
        return { trackData ->
            storagePermissionsRequest
                    .flatMap { filesProvider.getTracks() }
                    .doOnNext { player.playOrPause(trackData.filename) }
                    .flatMap { files -> progressListener.map { TracksViewState(files, TrackData(trackData.filename, it.first, it.second)) } }
        }
    }

    private val storagePermissionsRequest by lazy {
        TedRx2Permission.with(context)
                .setRationaleTitle(R.string.rationale_title)
                .setRationaleMessage(R.string.rationale_storage_text)
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .request()
                .toObservable()
    }

}
