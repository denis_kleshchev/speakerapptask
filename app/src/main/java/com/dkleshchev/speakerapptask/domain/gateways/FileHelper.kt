package com.dkleshchev.speakerapptask.domain.gateways

interface FileHelper {

    fun copyFile(input: String,
                 output: String)

    fun deleteFile(filename: String)

}
