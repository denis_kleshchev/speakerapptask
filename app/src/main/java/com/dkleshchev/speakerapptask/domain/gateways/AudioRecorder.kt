package com.dkleshchev.speakerapptask.domain.gateways

interface AudioRecorder {

    fun startRecord(filename: String)

    fun stopRecord()

}
