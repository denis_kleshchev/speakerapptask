package com.dkleshchev.speakerapptask.domain.interactors

import com.dkleshchev.speakerapptask.domain.boundaries.AudioRecorderApi
import com.dkleshchev.speakerapptask.domain.gateways.AudioRecorder
import com.dkleshchev.speakerapptask.domain.gateways.FileHelper
import com.dkleshchev.speakerapptask.domain.gateways.FilenameProvider
import com.dkleshchev.speakerapptask.presentation.di.qualifier.NewTrackAddedEvents
import com.dkleshchev.speakerapptask.presentation.main.model.ButtonEvent
import com.dkleshchev.speakerapptask.presentation.di.qualifier.StopPlaybackEvents
import com.dkleshchev.speakerapptask.presentation.main.MainViewState
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

class AudioRecordInteractor @Inject constructor(@StopPlaybackEvents private val stopPlaybackListener: PublishSubject<Any>,
                                                @NewTrackAddedEvents private val newFileEventsListener: PublishSubject<Any>,
                                                private val recorder: AudioRecorder,
                                                private val fileHelper: FileHelper,
                                                private val filenameProvider: FilenameProvider) : AudioRecorderApi {

    companion object {
        private const val recordDelay = 500L
        private val event = Any()

        private val nonRecordingState = MainViewState(false)
    }

    private val recordingTrigger = AtomicBoolean(false)

    override fun processButtonEvent(): (ButtonEvent) -> Observable<MainViewState> {
        return { event ->
            when (event) {
                ButtonEvent.PRESS -> Observable.fromCallable { stopPlaybackListener.onNext(event) }
                        .map { event }
                        .delay(recordDelay, TimeUnit.MILLISECONDS)
                        .doOnNext { recordingTrigger.set(true) }
                        .map { filenameProvider.getTempFileName() }
                        .doOnNext { recorder.startRecord(it) }
                        .flatMap { Observable.interval(0L, 1L, TimeUnit.SECONDS) }
                        .map { MainViewState(true, it.toString()) }
                else -> Observable.just(recordingTrigger.getAndSet(false))
                        .filter { it }
                        .doOnNext { recorder.stopRecord() }
                        .map { filenameProvider.getTempFileName() }
                        .doOnNext { fileHelper.copyFile(it, filenameProvider.getFileName()) }
                        .doOnNext { fileHelper.deleteFile(filenameProvider.getTempFileName()) }
                        .doOnNext { newFileEventsListener.onNext(event) }
                        .map { nonRecordingState }
            }
        }
    }


}
