package com.dkleshchev.speakerapptask.presentation.di.module

import com.dkleshchev.speakerapptask.data.AudioPlayerApiImplementation
import com.dkleshchev.speakerapptask.data.TracksListProviderImplementation
import com.dkleshchev.speakerapptask.domain.interactors.TracksProcessorInteractor
import com.dkleshchev.speakerapptask.domain.gateways.AudioPlayerApi
import com.dkleshchev.speakerapptask.domain.gateways.TracksListProvider
import com.dkleshchev.speakerapptask.domain.boundaries.TracksProcessor
import com.dkleshchev.speakerapptask.presentation.di.qualifier.CurrentProgressEvents
import com.dkleshchev.speakerapptask.presentation.main.tracks.TracksViewState
import io.reactivex.subjects.BehaviorSubject
import toothpick.config.Module

class TracksFragmentModule(initialState: TracksViewState) : Module() {

    init {
        bind(TracksViewState::class.java).toInstance(initialState)
        bind(TracksListProvider::class.java).to(TracksListProviderImplementation::class.java)
        bind(AudioPlayerApi::class.java).to(AudioPlayerApiImplementation::class.java)
        bind(BehaviorSubject::class.java).withName(CurrentProgressEvents::class.java).toInstance(BehaviorSubject.createDefault(Pair(0L, 0)))
        bind(TracksProcessor::class.java).to(TracksProcessorInteractor::class.java)
    }

}
