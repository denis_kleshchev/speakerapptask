package com.dkleshchev.speakerapptask.presentation.main

import android.Manifest
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.MotionEvent
import android.view.View
import com.dkleshchev.speakerapptask.R
import com.dkleshchev.speakerapptask.presentation.di.Scopes
import com.dkleshchev.speakerapptask.presentation.di.module.MainActivityModule
import com.dkleshchev.speakerapptask.presentation.main.model.ButtonEvent
import com.dkleshchev.speakerapptask.presentation.main.tracks.TracksFragment
import com.hannesdorfmann.mosby3.mvi.MviActivity
import com.jakewharton.rxbinding2.view.RxView
import com.tedpark.tedpermission.rx2.TedRx2Permission
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import toothpick.Toothpick
import kotlin.properties.Delegates

class MainActivity : MviActivity<MainView, MainPresenter>(), MainView {

    companion object {
        private const val vibrationDuration = 100L
    }

    private val scope = Toothpick.openScopes(Scopes.appScope, Scopes.mainActivityScope)
    private val presenter by lazy { scope.getInstance(MainPresenter::class.java) }

    private var isRecording by Delegates.observable(false) { _, _, newValue ->
        val visibility = if (newValue) View.VISIBLE else View.GONE
        recordingMask.visibility = visibility
    }

    private val micPermissionsRequest by lazy {
        TedRx2Permission.with(this)
                .setRationaleTitle(R.string.rationale_title)
                .setRationaleMessage(R.string.rationale_mic_text)
                .setPermissions(Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .request()
    }

    override fun createPresenter(): MainPresenter = presenter

    override val buttonEvents: Observable<ButtonEvent>
        get() = RxView.touches(fab)
                .filter { it.action == MotionEvent.ACTION_DOWN || it.action == MotionEvent.ACTION_UP }
                .flatMap { event ->
                    if (event.action == MotionEvent.ACTION_DOWN) {
                        micPermissionsRequest
                                .toObservable()
                                .map { event }
                    } else {
                        Observable.just(event)
                    }
                }
                .map {
                    when (it.action) {
                        MotionEvent.ACTION_DOWN -> ButtonEvent.PRESS
                        MotionEvent.ACTION_UP -> ButtonEvent.RELEASE
                        else -> ButtonEvent.NONE //should not happen
                    }
                }
                .doOnNext { vibrate() }

    override fun onCreate(savedInstanceState: Bundle?) {
        initScopeAndInject()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, TracksFragment.newInstance())
                    .commit()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        if (isFinishing) {
            Toothpick.closeScope(Scopes.mainActivityScope)
        }
    }

    override fun render(viewState: MainViewState) {
        isRecording = viewState.isRecording
        recordingMask.text = getString(R.string.record_text, viewState.recordTime)
    }

    private fun vibrate() {
        (getSystemService(Context.VIBRATOR_SERVICE) as Vibrator)
                .let {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        it.vibrate(VibrationEffect.createOneShot(vibrationDuration, VibrationEffect.DEFAULT_AMPLITUDE))
                    } else {
                        it.vibrate(vibrationDuration)
                    }
                }
    }

    private fun initScopeAndInject() {
        scope.installModules(MainActivityModule(MainViewState(false)))
        Toothpick.inject(this, scope)
    }
}
