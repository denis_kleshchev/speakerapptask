package com.dkleshchev.speakerapptask.presentation.di

object Scopes {

    const val appScope = "application scope"
    const val mainActivityScope = "main activity scope"
    const val tracksFragmentScope = "tracks fragment scope"

}
