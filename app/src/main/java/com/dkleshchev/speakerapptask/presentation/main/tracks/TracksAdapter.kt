package com.dkleshchev.speakerapptask.presentation.main.tracks

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.dkleshchev.speakerapptask.R
import com.dkleshchev.speakerapptask.presentation.main.model.TrackData
import com.dkleshchev.speakerapptask.presentation.main.model.TrackItem
import kotlin.properties.Delegates

class TracksAdapter(private val playListener: (String) -> Unit,
                    private val progressListener: (Long) -> Unit) :
        RecyclerView.Adapter<TracksViewHolder>() {

    var items by Delegates.observable(listOf<TrackItem>()) { _, _, _ -> notifyDataSetChanged() }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TracksViewHolder =
            TracksViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_record, parent, false))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: TracksViewHolder, position: Int) {
        holder.bind(items[position], playListener, progressListener)
    }

    fun update(item: TrackData) {
        items.find { it.filename == item.filename }?.let {
            it.isPlaying = true
            it.trackDuration = item.duration
            it.progress = item.progress.toInt()
            notifyItemChanged(items.indexOf(it))
        }
    }

}
