package com.dkleshchev.speakerapptask.presentation.main

import com.dkleshchev.speakerapptask.domain.boundaries.AudioRecorderApi
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class MainPresenter @Inject constructor(private val recorder: AudioRecorderApi,
                                        initialViewState: MainViewState) :
        MviBasePresenter<MainView, MainViewState>(initialViewState) {

    override fun bindIntents() {
        val intent = intent(MainView::buttonEvents)
                .switchMap(recorder.processButtonEvent())
                .observeOn(AndroidSchedulers.mainThread())

        subscribeViewState(intent, MainView::render)
    }

}