package com.dkleshchev.speakerapptask.presentation.main.model

data class TrackData(val filename: String,
                     val progress: Long,
                     val duration: Int)