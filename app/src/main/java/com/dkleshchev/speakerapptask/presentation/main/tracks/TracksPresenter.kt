package com.dkleshchev.speakerapptask.presentation.main.tracks

import com.dkleshchev.speakerapptask.domain.boundaries.TracksProcessor
import com.dkleshchev.speakerapptask.presentation.main.model.TrackData
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class TracksPresenter @Inject constructor(private val processor: TracksProcessor,
                                          initialViewState: TracksViewState) :
        MviBasePresenter<TracksView, TracksViewState>(initialViewState) {


    override fun bindIntents() {
        subscribeViewState(Observable.combineLatest(intent(TracksView::playButtonClicks),
                intent(TracksView::progressSeek),
                BiFunction<String, Long, TrackData> { click, seek -> TrackData(click, seek,0) })
                .switchMap (processor.processEvents())
                .observeOn(AndroidSchedulers.mainThread()),
                TracksView::render)
    }

}
