package com.dkleshchev.speakerapptask.presentation

import android.app.Application
import com.dkleshchev.speakerapptask.presentation.di.Scopes
import com.dkleshchev.speakerapptask.presentation.di.module.AppModule
import toothpick.Toothpick
import toothpick.configuration.Configuration

class SpeakerApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initToothpick()
    }

    private fun initToothpick() {
        Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
        Toothpick.openScope(Scopes.appScope).run {
            installModules(AppModule(this@SpeakerApplication))
        }
    }

}
