package com.dkleshchev.speakerapptask.presentation.main.model

enum class ButtonEvent {

    PRESS, RELEASE, NONE

}
