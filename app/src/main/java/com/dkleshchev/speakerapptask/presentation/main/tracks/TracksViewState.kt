package com.dkleshchev.speakerapptask.presentation.main.tracks

import com.dkleshchev.speakerapptask.presentation.main.model.TrackData
import com.dkleshchev.speakerapptask.presentation.main.model.TrackItem

data class TracksViewState(val tracks: List<TrackItem>,
                           val currentTrack: TrackData? = null)
