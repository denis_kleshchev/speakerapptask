package com.dkleshchev.speakerapptask.presentation.di.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class CurrentProgressEvents
