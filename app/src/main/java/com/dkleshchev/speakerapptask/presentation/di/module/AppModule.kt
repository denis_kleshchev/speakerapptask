package com.dkleshchev.speakerapptask.presentation.di.module

import android.content.Context
import com.dkleshchev.speakerapptask.presentation.SpeakerApplication
import toothpick.config.Module

class AppModule (app: SpeakerApplication) : Module() {

    init {
        bind(Context::class.java).toInstance(app)
    }

}
