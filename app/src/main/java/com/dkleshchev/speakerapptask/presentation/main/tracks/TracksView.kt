package com.dkleshchev.speakerapptask.presentation.main.tracks

import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.Observable

interface TracksView : MvpView {

    val playButtonClicks: Observable<String>
    val progressSeek: Observable<Long>
    fun render(viewState: TracksViewState)

}
