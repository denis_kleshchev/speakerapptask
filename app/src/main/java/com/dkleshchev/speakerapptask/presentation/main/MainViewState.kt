package com.dkleshchev.speakerapptask.presentation.main

data class MainViewState(val isRecording: Boolean,
                         val recordTime: String = "")
