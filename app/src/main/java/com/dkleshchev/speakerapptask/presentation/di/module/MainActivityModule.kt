package com.dkleshchev.speakerapptask.presentation.di.module

import com.dkleshchev.speakerapptask.data.FileHelperImplementation
import com.dkleshchev.speakerapptask.data.FilenameProviderImplementation
import com.dkleshchev.speakerapptask.data.SimpleAudioRecorderImplementation
import com.dkleshchev.speakerapptask.domain.boundaries.AudioRecorderApi
import com.dkleshchev.speakerapptask.domain.gateways.AudioRecorder
import com.dkleshchev.speakerapptask.domain.gateways.FileHelper
import com.dkleshchev.speakerapptask.domain.gateways.FilenameProvider
import com.dkleshchev.speakerapptask.domain.interactors.AudioRecordInteractor
import com.dkleshchev.speakerapptask.presentation.di.qualifier.NewTrackAddedEvents
import com.dkleshchev.speakerapptask.presentation.di.qualifier.StopPlaybackEvents
import com.dkleshchev.speakerapptask.presentation.main.MainViewState
import io.reactivex.subjects.PublishSubject
import toothpick.config.Module

class MainActivityModule(initialState: MainViewState) : Module() {

    init {
        bind(MainViewState::class.java).toInstance(initialState)
        bind(PublishSubject::class.java).withName(StopPlaybackEvents::class.java).toInstance(PublishSubject.create<Any>())
        bind(AudioRecorderApi::class.java).to(AudioRecordInteractor::class.java)
        bind(AudioRecorder::class.java).to(SimpleAudioRecorderImplementation::class.java)
        bind(FileHelper::class.java).to(FileHelperImplementation::class.java)
        bind(FilenameProvider::class.java).to(FilenameProviderImplementation::class.java)
        bind(PublishSubject::class.java).withName(NewTrackAddedEvents::class.java).toInstance(PublishSubject.create<Any>())
    }

}
