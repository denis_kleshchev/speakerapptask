package com.dkleshchev.speakerapptask.presentation.main.tracks

import android.Manifest
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SimpleItemAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dkleshchev.speakerapptask.R
import com.dkleshchev.speakerapptask.presentation.di.Scopes
import com.dkleshchev.speakerapptask.presentation.di.module.TracksFragmentModule
import com.hannesdorfmann.mosby3.mvi.MviFragment
import com.tedpark.tedpermission.rx2.TedRx2Permission
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.fragment_playlist.*
import toothpick.Toothpick

class TracksFragment : MviFragment<TracksView, TracksPresenter>(), TracksView {

    companion object {
        fun newInstance() = TracksFragment()
    }

    private val scope = Toothpick.openScopes(Scopes.appScope, Scopes.mainActivityScope, Scopes.tracksFragmentScope)
    private val presenter by lazy { scope.getInstance(TracksPresenter::class.java) }

    private val playButtonsClicksSubject = BehaviorSubject.createDefault("")
    private val progressSeekSubject = BehaviorSubject.createDefault(0L)

    private val adapter by lazy {
        TracksAdapter({ playButtonsClicksSubject.onNext(it) },
                { progressSeekSubject.onNext(it) })
    }

    override fun createPresenter(): TracksPresenter = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        initScopeAndInject()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            view ?: inflater.inflate(R.layout.fragment_playlist, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        trackList.apply {
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            adapter = this@TracksFragment.adapter
        }
    }

    override val playButtonClicks: Observable<String>
        get() = playButtonsClicksSubject
    override val progressSeek: Observable<Long>
        get() = progressSeekSubject

    override fun render(viewState: TracksViewState) {
        adapter.items = viewState.tracks
        viewState.currentTrack?.let { adapter.update(it) }
        emptyMask.visibility = if (viewState.tracks.isEmpty()) View.VISIBLE else View.GONE
    }

    private fun initScopeAndInject() {
        scope.installModules(TracksFragmentModule(TracksViewState(listOf())))
        Toothpick.inject(this, scope)
    }

}
