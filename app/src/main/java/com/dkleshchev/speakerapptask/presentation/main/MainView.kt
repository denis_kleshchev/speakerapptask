package com.dkleshchev.speakerapptask.presentation.main

import com.dkleshchev.speakerapptask.presentation.main.model.ButtonEvent
import com.hannesdorfmann.mosby3.mvp.MvpView
import io.reactivex.Observable

interface MainView : MvpView {

    val buttonEvents: Observable<ButtonEvent>
    fun render(viewState: MainViewState)

}