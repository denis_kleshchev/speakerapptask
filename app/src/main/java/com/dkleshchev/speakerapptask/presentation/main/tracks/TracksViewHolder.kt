package com.dkleshchev.speakerapptask.presentation.main.tracks

import android.support.v7.widget.RecyclerView
import android.view.MotionEvent
import android.view.View
import android.widget.SeekBar
import com.dkleshchev.speakerapptask.R
import com.dkleshchev.speakerapptask.logInfo
import com.dkleshchev.speakerapptask.presentation.main.model.TrackItem
import kotlinx.android.synthetic.main.item_record.view.*
import java.util.concurrent.atomic.AtomicBoolean

class TracksViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    private val isUserInput = AtomicBoolean(false)

    fun bind(item: TrackItem,
             playListener: (String) -> Unit,
             progressListener: (Long) -> Unit) = view.apply {
        title.text = item.title
        playbackProgress.apply {
            isEnabled = item.isPlaying
            max = item.trackDuration
            progress = item.progress
            setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    if (isUserInput.get()) {
                        progressListener(progress.toLong())
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {

                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {

                }
            })

            setOnTouchListener { _, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> isUserInput.set(true)
                    MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> isUserInput.set(false)
                }
                false
            }
        }

        actionButton.apply {
            setOnClickListener {
                logInfo("item ${item.title} click")
                playListener(item.filename)
            }
            setImageResource(if (item.isPlaying) R.drawable.ic_pause else R.drawable.ic_play)
        }
    }

}
