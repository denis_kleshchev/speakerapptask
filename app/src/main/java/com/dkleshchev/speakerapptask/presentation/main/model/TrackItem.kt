package com.dkleshchev.speakerapptask.presentation.main.model

data class TrackItem(val title: String,
                     val filename: String,
                     var isPlaying: Boolean,
                     var progress: Int,
                     var trackDuration: Int)