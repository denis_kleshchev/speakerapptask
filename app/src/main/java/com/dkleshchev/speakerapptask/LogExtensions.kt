package com.dkleshchev.speakerapptask

import org.slf4j.Logger
import org.slf4j.LoggerFactory

internal fun Any.lazyLogger(): Lazy<Logger> = lazy { LoggerFactory.getLogger(javaClass.simpleName) }

internal fun Any.logInfo(message: String) = lazyLogger().value.info(message)
internal fun Any.logDebug(message: String) = lazyLogger().value.debug(message)
internal fun Any.logWarn(message: String) = lazyLogger().value.warn(message)
internal fun Any.logError(message: String,
                 throwable: Throwable? = null) = lazyLogger().value.error(message, throwable)
